from django.shortcuts import render
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["price","automobile","sales_person", "customer", "id"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET"])
def api_list_autos_vo(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse({"autos": autos}, encoder=AutomobileVOEncoder)




@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse({
            "salespeople": salespeople},
            encoder=SalespersonEncoder
            )
    else:
        content = json.loads(request.body)
        try:
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
                status=201
            )
        except:
            return JsonResponse(
                {"salesperson creation failed": "Invalid data"},
                status = 400
            )


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({
                "Error": "This salesperson was not found"},
                status = 404)
    else:
        try:
            count, _ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse({
                "deleted": count > 0
            })
        except:
            return JsonResponse({
                "Error": "Failed to delete salesperson"},
                status = 500)

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({
            "customers": customers},
            encoder=CustomerEncoder
            )
    else:
        content = json.loads(request.body)
        try:
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
                status=201
            )
        except:
            return JsonResponse(
                {"Error": "Customer Creation Failed"},
                status = 400
            )

@require_http_methods(["GET", "DELETE"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({
                "Error": "This customer was not found"},
                status = 404)
    else:
        try:
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({
                "deleted": count > 0
            })
        except:
            return JsonResponse({
                "Error": "Failed to delete customer"},
                status = 500)





@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({
            "sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content=json.loads(request.body)

        vin = content['automobile']
        automobile = AutomobileVO.objects.get(vin=vin)
        content["automobile"] = automobile

        sales_person = content['sales_person']
        get_customer = Salesperson.objects.get(id=sales_person)
        content["sales_person"] = get_customer

        customer = content["customer"]
        get_customer = Customer.objects.get(id=customer)
        content["customer"] = get_customer


        automobile.sold = True
        automobile.save()
    try:
        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
            status=201)
    except:
        return JsonResponse(
                    {"Error": "Sale Creation Failed"},
                    status = 400
                )




@require_http_methods(["GET", "DELETE"])
def api_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse({
                "Error": "This sale record was not found"},
                status = 404)
    else:
        try:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({
                "deleted": count > 0
            })
        except:
            return JsonResponse({
                "Error": "Sales record Deletion Failed"},
                status = 500)
